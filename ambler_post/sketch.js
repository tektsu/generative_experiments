// Canvas size
const height = 200;
const width = 200;

const startX = 25;
const startY = 25;
const length = 150;

function setup() {
    createCanvas(width, height);
    background(255);
}

function draw() {
    strokeWeight(3);
    line(startX+0, startY+0, startX+length, startY+0);
    line(startX+length, startY+0, startX+length, startY+length);
    line(startX+length, startY+length, startX+0, startY+length);
    line(startX+0, startY+length, startX+0, startY+0);

    strokeWeight(1);
    stroke('grey');
    const incr=length/6;
    for(let i=startX; i<startX+length; i+=incr) {
        line(i, startY+0, i, startY+length);
        line(startX+0, i, startX+length, i);
    }

    strokeWeight(3);
    stroke("red");
    line(startX+incr, startY+incr, startX+incr, startY+5*incr);
    line(startX+incr, startY+5*incr, startX+5*incr, startY+5*incr);
    line(startX+5*incr, startY+5*incr, startX+5*incr, startY+incr);
    line(startX+5*incr, startY+incr, startX+2*incr, startY+incr);
    line(startX+2*incr, startY+incr, startX+2*incr, startY+4*incr);
    line(startX+2*incr, startY+4*incr, startX+4*incr, startY+4*incr);
    line(startX+4*incr, startY+4*incr, startX+4*incr, startY+2*incr);
    line(startX+4*incr, startY+2*incr, startX+3*incr, startY+2*incr);
    line(startX+3*incr, startY+2*incr, startX+3*incr, startY+3*incr);
    noLoop();
}
