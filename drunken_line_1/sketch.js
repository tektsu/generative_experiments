const s = ( p ) => {
    // Canvas size
    const height = 600;
    const width = 600;

    // Initial starting point
    let x = width / 2;
    let y = height / 2;

    // Maximum amount to move in both x and y
    const posIncr = 10;

    // Initial color
    let red = 0;  // Red
    let green = 0;  // Green
    let blue = 0;  // Blue

    // Maximum amount to change each color element for each line
    const colorIncr = 5;


    p.setup = function() {
        p.createCanvas(width, height);
        p.background(0);
        x = p.random(0, width - 1);
        y = p.random(0, height - 1);
        p.stroke(red, green, blue);
    };

    p.draw = function() {
        let x1 = p.randomChange(x, posIncr, 0, width - 1)
        let y1 = p.randomChange(y, posIncr, 0, height - 1)
        p.line(x, y, x1, y1);
        x = x1;
        y = y1;

        red = p.randomChange(red, colorIncr, 0, 255);
        green = p.randomChange(green, colorIncr, 0, 255);
        blue = p.randomChange(blue, colorIncr, 0, 255);
        p.stroke(red, green, blue);
    };

    p.randomChange = function(oldValue, increment, min, max) {
        let newValue = oldValue + Math.floor(p.random(-increment, increment+1));
        if (newValue < min) {
            newValue += increment;
        }
        if (newValue > max) {
            newValue -= increment;
        }

        return newValue;
    };
};

let sketch = new p5(s);
