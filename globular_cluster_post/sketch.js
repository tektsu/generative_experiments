// Canvas size
const height = 600;
const width = 600;
const xIncr = 2;
const yIncr = 5;

// Initial starting point
let rows = [];

function setup() {
    createCanvas(width, height);
    background(0);
    stroke(128, 0, 0);

    let y = yIncr;
    while (y < height) {
        let x = 0;
        let row = [{x: x, y: y}];
        while (x < width) {
            x += xIncr;
            row.push({x: x, y: modifyY(x, y)});
        }
        rows.push(row);
        y += yIncr;
    }
}

function draw() {
    for (let r=0; r<rows.length; r++) {
        for (let c=1; c<rows[r].length; c++) {
                line(rows[r][c-1]['x'], rows[r][c-1]['y'], rows[r][c]['x'], rows[r][c]['y']);
        }
    }
    for (let r=0; r<rows.length; r++) {
        for (let c=0; c<rows[r].length; c++) {
            let radius = Math.abs(rows[r][0]['y']-rows[r][c]['y']);
            if(radius > 3) {
                circle(rows[r][c]['x'], rows[r][c]['y'], radius);
            }
        }
    }

    noLoop();
}

function modifyY(x, y) {

    let maxEntropy = {
        x: width/2,
        y: height/2,
        width: width/2,
        height: height/2,
    };
    let entropyX = Math.max((maxEntropy['width'] - Math.abs(x - maxEntropy['x'])) / maxEntropy['width'], 0);
    let entropyY = Math.max((maxEntropy['height'] - Math.abs(y - maxEntropy['y'])) / maxEntropy['height'], 0);

    let entropy = entropyX * entropyY;

    let newY = y + Math.floor(randomGaussian(0, yIncr * entropy));

    return newY;
}



