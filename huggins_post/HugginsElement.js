const height = 200;
const width = 200;

function setup() {
    createCanvas(width, height);
    background(255);
    strokeWeight(2);

    const radius = 20;
    const p1 = new Point(66, 100);
    const p2 = new Point(134, 100);

    fill(0);
    circle(p1.x, p1.y, radius*2);
    circle(p2.x, p2.y, radius*2);

    const x1 = new Polar(radius, radians(225)).toPointCenter(p1);
    const x2 = new Polar(radius, radians(315)).toPointCenter(p2);

    const c1 = new Polar(5*radius, radians(135)).toPointCenter(p1);
    const c2 = new Polar(5*radius, radians(45)).toPointCenter(p2);

    fill(0, 0, 0, 0);
    stroke(255, 0, 0);
    curve(c1.x, c1.y, x1.x, x1.y, x2.x, x2.y, c2.x, c2.y);
}
