const height = 200;
const width = 400;

function setup() {
    createCanvas(width, height);
    background(255);
    const h = new Huggins([
        new Point(0, 0),
        new Point( width/2, 0),
        new Point(width/2, height),
        new Point(0, height),
    ], {
    });
    const w = new W2([
        new Point(width/2, 0),
        new Point( width, 0),
        new Point(width, height),
        new Point(width/2, height),
    ], {
    });
    image(h.g, 0, 0);
    image(w.g, width/2, 0);

    fill('red')
    textSize(20);
    text("Huggins", 20, height-33);
    text("W2", width/2+20, height-33);
}
