// Canvas size
const height = 1000;
const width = 1000;


let img;
let slice;
let originX = 0;
let originY = 0;
function preload() {
    img = loadImage('/sb2010101201.jpg');
}

function setup() {
    createCanvas(width, height);
    background(240);

    slice = createGraphics(width, height);
    slice.noStroke();
    slice.beginShape();
    slice.vertex(originX, originY);
    slice.vertex(500+originX,originY);
    slice.vertex(250+originX, 500*.87+originY);
    slice.endShape(CLOSE);

    angleMode(DEGREES);
}

function draw() {
    img.mask(slice);
    // translate(-originX, -originY);
    // rotate(45);
    image(img, width/2, height/2);
    rotate(45);
    image(img, width/2, height/2);
    fill(0);
    stroke(0);
    circle(originX, originY, 10);
    noLoop();
}
