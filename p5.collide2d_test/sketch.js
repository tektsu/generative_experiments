//example adapted from Jeffrey Thompson
var hit = false;
var poly = new Array(8);
var randomPoly = new Array(8);
function setup() {
    createCanvas(windowWidth,windowHeight);
    //collideDebug(true) //enable debug mode

    //generate a uniform sided polygon
    var angle = TWO_PI / poly.length;
    for (var i=0; i<poly.length; i++) {
        var a = angle * i;
        var x = 300 + cos(a) * 100;
        var y = 200 + sin(a) * 100;
        poly[i] = createVector(x,y);
    }

    // create a 2nd larger polygon
    for (var i=0; i<randomPoly.length; i++) {
        var a = angle * i;
        var x = 300 + cos(a) * 150;
        var y = 200 + sin(a) * 150;
        randomPoly[i] = createVector(x,y);
    }
}
function draw() {
    background(255);
    fill(128, 0, 0, 128);
    // update random polygon to mouse position
    var mouse = createVector(mouseX, mouseY);
    var diff = mouse.sub(randomPoly[0]);

    for (i=0; i < randomPoly.length; i++) {
        randomPoly[i].add(diff);
    }

    beginShape();
    for(i=0; i < randomPoly.length; i++){
        vertex(randomPoly[i].x,randomPoly[i].y);
    }
    endShape(CLOSE);
    beginShape();
    //draw the polygon from the created Vectors above.
    for(i=0; i < poly.length; i++){
        vertex(poly[i].x,poly[i].y);
    }
    endShape(CLOSE);

    hit = collidePolyPoly(poly,randomPoly,true);

    stroke( (hit) ? color("red") : 0);
    print("colliding? " + hit);

}
