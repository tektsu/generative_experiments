const height = 600;
const width = 600;

const xSpacing = 100;
const ySpacing = 100;
let colors;
function setup() {
    createCanvas(width, height);
    background(240);

    colors = [
        color(0),
        color(100),
        color(200),
        color(255),
        color(128, 128, 0),
        color(200, 200, 255),
        color(128, 50, 128),
        color(255, 165, 0),
        color("#fc0"),
        color("#65c323"),
        color("#800000"),
        color("#123456"),
        "rgb(50%, 50%, 0%)",
        "rgb(78%, 78%, 100%)",
        "rgb(50%, 20%, 50%)",
        "rgb(100%, 65%, 0%)",
        "hsb(45, 70%, 70%)",
        "hsb(135, 80%, 100%)",
        "hsb(225, 90%, 0%)",
        "hsb(315, 100%, 100%)",
        "hsl(0, 50%, 50%)",
        "hsl(90, 25%, 30%)",
        "hsl(180, 20%, 50%)",
        "hsl(270, 65%, 40%)",
        "lightsalmon",
        "teal",
        "yellowgreen",
        "antiquewhite",
        "cornflowerblue",
        "dimgrey",
        "deeppink",
        "lavenderblush",
        "mediumturquoise",
        "midnightblue",
        "chocolate",
        "coral",
    ];
}

function draw() {

    noStroke();
    for(let y=0; y<height; y+=ySpacing) {
        for (let x=0; x<width; x+=xSpacing) {
            let color = colors.shift();
            if (color === undefined) {
                color = 'white';
            }
            fill(color);
            circle(x+xSpacing/2, y+ySpacing/2, Math.min(xSpacing, ySpacing)*.8);
        }
    }

    noLoop();
}

